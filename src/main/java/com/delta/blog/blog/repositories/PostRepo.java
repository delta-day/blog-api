package com.delta.blog.blog.repositories;

import com.delta.blog.blog.models.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

//not a class, change to interface and inherit JpaR
public interface PostRepo extends JpaRepository<Post, Long> {
    






}
