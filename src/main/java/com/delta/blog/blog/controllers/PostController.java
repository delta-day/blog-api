package com.delta.blog.blog.controllers;

import java.util.List;

import com.delta.blog.blog.models.Post;
import com.delta.blog.blog.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*") 
@RestController // identifies the class as a controller
@RequestMapping("/api/v1") //url to make a request to methods below
public class PostController {
    
 
    @Autowired
    PostService pService;

    //create Get method to Get the data from our database, table names "posts"
    @GetMapping("/posts")
    public ResponseEntity<List<Post>> get(){
        List<Post> posts = pService.findAll();

        return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
    }

}
