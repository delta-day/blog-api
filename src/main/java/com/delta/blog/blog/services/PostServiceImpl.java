package com.delta.blog.blog.services;

import java.util.List;

import com.delta.blog.blog.models.Post;
import com.delta.blog.blog.repositories.PostRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {




    @Autowired
    PostRepo postRepo;

    @Override
    public List<Post> findAll() {
       
        return postRepo.findAll();
    }




    
}
